'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstname: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
        comment: 'Email Address',
        validate: {
          isEmail: {
            msg: 'Please enter a valid email'
          },
          notNull: {
            args: true,
            msg: "Required"
          },
        }
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
        comment: 'Password',
        validate: {
          min: {
            args: 8,
            msg: "Must be 8 or more characters"
          },
          notNull: {
            args: true,
            msg: "Required"
          },
          isAlphanumeric: {
            args: true,
            msg: "At last 1 number & 1 letter"
          },
        }
      },
      nickname: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      lastname: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      secondlastname: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      identification_name: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      identification_code: {
        // allowNull: false,
        type: Sequelize.STRING
      },
      phone: {
        // allowNull: false,
        type: Sequelize.BIGINT
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Users');
  }
};